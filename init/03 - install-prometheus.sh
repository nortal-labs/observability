#!/bin/bash

# Long installation
echo "This may take 1-2 minutes, be patient!!"
helm upgrade prometheus --recreate-pods --install stable/prometheus-operator --namespace monitoring
# helm install stable/prometheus-operator --namespace monitoring --name prometheus
