#!/bin/bash

# Init Helm
helm init --service-account tiller

## Verify Installation
kubectl -n kube-system  rollout status deploy/tiller-deploy

# Not sure why I have to patch deploy the serviceAccount to tiller, assumed that was already done
kubectl patch deploy --namespace kube-system tiller-deploy -p '{"spec":{"template":{"spec":{"serviceAccount":"tiller"}}}}'

# Re-init Helm
helm init --upgrade --service-account tiller

## Add public repo list to helm
helm repo add stable https://kubernetes-charts.storage.googleapis.com/
