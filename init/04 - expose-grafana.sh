#!/bin/bash

DEPLOY_NAME=$(kubectl get svc --namespace monitoring -l "app=grafana,release=prometheus" -o jsonpath="{.items[0].metadata.name}")
SVC_NAME="service/${DEPLOY_NAME}"

PASS=$(kubectl get secret --namespace monitoring prometheus-grafana -o jsonpath="{.data.admin-password}" | base64 --decode ; echo)
USER=$(kubectl get secret --namespace monitoring prometheus-grafana -o jsonpath="{.data.admin-user}" | base64 --decode ; echo)

echo ""
echo "Username: ${USER}"
echo "Password: ${PASS}"
echo ""

kubectl port-forward -n monitoring ${SVC_NAME} 3000:80
